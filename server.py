from flask import Flask
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pprint import pprint

scope = ["https://spreadsheets.google.com/feeds",'https://www.googleapis.com/auth/spreadsheets',"https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]

creds = ServiceAccountCredentials.from_json_keyfile_name("creds.json", scope)
client = gspread.authorize(creds)
sheet = client.open("gummi_schlong_data").sheet1

insertRow = ["hello", 5, "re", "blue"]
sheet.append_row(insertRow)


app = Flask(__name__)
'''
@app.route('/input', methods=["POST"])
def input_data():
    return 'Hello, World!'
'''